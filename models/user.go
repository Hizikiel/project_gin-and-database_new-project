package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	
	

	"unicode"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/joomcode/errorx"
)


	


var (
	ErrorNamespace     = errorx.NewNamespace("error_handlers")
	ErrInvalidRequest  = ErrorNamespace.NewType("invalid_request")
	ErrDatabase        = ErrorNamespace.NewType("database_error")
	ErrUserExists      = ErrorNamespace.NewType("user_exists")
	ErrUnauthorized    = ErrorNamespace.NewType("unauthorized")
	ErrTokenGeneration = ErrorNamespace.NewType("token_generation_error")
	ErrTokenRefresh    = ErrorNamespace.NewType("token_refresh_error")
	ErrInternalServer  = ErrorNamespace.NewType("internal_server_error")
)

type Image struct {
	ID   uint   `gorm:"primaryKey"`
	Name string `gorm:"uniqueIndex"`
	Data []byte
}
type MetaData struct {
	Page    int `json:"page"`
	PerPage int `json:"per_page"`
}
type SuccessResponse struct {
	MetaData MetaData    `json:"meta_data"`
	Data     interface{} `json:"data"`
}
type PhoneNumber string

func (p *PhoneNumber) UnmarshalJSON(data []byte) error {
	var temp string
	if err := json.Unmarshal(data, &temp); err != nil {
		return err
	}
	*p = PhoneNumber(temp)
	return nil
}

type User struct {
	ID          uint        `gorm:"primaryKey" json:"id"`
	Username    string      `json:"username"`
	FirstName   string      `json:"firstName"`
	LastName    string      `json:"lastName"`
	Password    string      `json:"password"`
	PhoneNumber PhoneNumber `json:"phonenumber"`
	Address     string      `json:"address"`
	Email       string      `json:"email"`
	Image       string      `json:"image"`
}

type Phone struct {
	Number string
}

func (p *Phone) UnmarshalJSON(data []byte) error {
	if string(data) == "null" || string(data) == `""` {
		err := errors.New("phoneNumber is required")
		return err
	}
	var phoneNumber string
	if err := json.Unmarshal(data, &phoneNumber); err != nil {
		return err
	}
	formatedPhone, err := FormatPhoneNumber(phoneNumber)
	if err != nil {
		return err
	}
	p.Number = formatedPhone
	return nil
}
func FormatPhoneNumber(phone string) (string, error) {
	// if isValid := PhoneReg.MatchString(phone); !isValid {
	// 	err := fmt.Errorf("invalid phone number format.Please enter a valid phone number")
	// 	return "", err
	// }
	reg := regexp.MustCompile(`[^\d]`)
	phone = reg.ReplaceAllString(phone, "")
	if phone[:1] == "0" {
		phone = phone[1:]
	}
	if phone[:3] != "251" {
		phone = "251" + phone
	}
	return phone, nil
}
func (u User) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required, validation.Length(5, 20)),
		validation.Field(&u.Password, validation.Required, validation.Length(6, 100)),
		validation.Field(&u.PhoneNumber, validation.Required, validation.By(validatePhoneNumber)),
		validation.Field(&u.Address, validation.Required, validation.By(validateAddress)),
		validation.Field(&u.Email, validation.Required, is.Email),
	)
}

func validatePhoneNumber(value interface{}) error {
	phoneNumber, _ := value.(PhoneNumber)
	phoneRegex := `^\d{10}$`
	if !regexp.MustCompile(phoneRegex).MatchString(string(phoneNumber)) {
		return fmt.Errorf("invalid phone number format")
	}
	return nil
}
func validateAddress(value interface{}) error {
	address, _ := value.(string)
	for _, char := range address {
		if unicode.IsDigit(char) {
			return fmt.Errorf("address cannot contain numbers")
		}
	}
	return nil
}
