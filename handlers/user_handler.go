package handlers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"third_project/db"
	"third_project/models"
	"third_project/utils"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)

var jwtKey = []byte("hzk")
var refreshKey = []byte("geb")

type Response struct {
	Email        string `json:"email"`
	Username     string `json:"username"`
	Token        string `json:"token"`
	RefreshToken string `json:"refresh_token"`
}

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type PaginationParams struct {
	Page     int    `json:"page"`
	PageSize int    `json:"page_size"`
	SortBy   string `json:"sort_by"`
}

type UserStorage struct {
	Store db.Database
}

func NewUserStorage(s db.Database) *UserStorage {
	return &UserStorage{
		Store: s,
	}
}

func (u *UserStorage) LoginHandler(c *gin.Context) {

	var creds Credentials
	ctx := c.Request.Context()
	if err := c.BindJSON(&creds); err != nil {
		err = errorx.Decorate(models.ErrInvalidRequest.New("invalid JSON payload"), "Invalid request")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, ok, err := u.Store.GetUser(ctx, creds.Username)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrDatabase.Wrap(err, "error retrieving user from database"), "Database error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		// c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}
	if !ok || user.Password != creds.Password {
		err = errorx.Decorate(models.ErrUnauthorized.New("invalid credentials"), "Invalid username or password")
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	expirationTime := time.Now().Add(5 * time.Minute)
	refreshExpirationTime := time.Now().Add(24 * time.Hour)

	claims := &jwt.StandardClaims{
		Id:        fmt.Sprintf("%d", user.ID),
		ExpiresAt: expirationTime.Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrTokenGeneration.Wrap(err, "error generating token"), "Token generation error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}

	refreshClaims := &jwt.StandardClaims{
		ExpiresAt: refreshExpirationTime.Unix(),
	}
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshClaims)
	refreshTokenString, err := refreshToken.SignedString(refreshKey)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrTokenGeneration.Wrap(err, "error generating refresh token"), "Refresh token generation error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		// c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}

	c.SetCookie("token", tokenString, int(expirationTime.Sub(time.Now()).Seconds()), "/", "", false, true)
	c.SetCookie("refresh_token", refreshTokenString, int(refreshExpirationTime.Sub(time.Now()).Seconds()), "/", "", false, true)
	response := Response{
		Email:        user.Email,
		Username:     user.Username,
		Token:        tokenString,
		RefreshToken: refreshTokenString,
	}

	c.JSON(http.StatusOK, response)
}

func (u *UserStorage) RegisterHandler(c *gin.Context) {
	ctx := c.Request.Context()
	var newUser models.User
	if err := c.BindJSON(&newUser); err != nil {
		err = errorx.Decorate(models.ErrInvalidRequest.New("invalid JSON payload"), "Invalid request")
		c.Error(err)
		// c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	_, exists, err := u.Store.GetUser(ctx, newUser.Username)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrDatabase.Wrap(err, "error checking if user exists in database"), "Database error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		// c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}
	if exists {
		err = errorx.Decorate(models.ErrUserExists.New("username already exists"), "Username already exists")
		log.Printf("Error: %+v", err)
		c.JSON(http.StatusConflict, gin.H{"error": err.Error()})
		return
	}

	err = u.Store.AddUser(ctx, newUser)
	if err != nil {
		// wrappedErr := errorx.Decorate(ErrDatabase.Wrap(err, "error adding new user to database"), "Database error")
		// log.Printf("Error: %+v", wrappedErr)
		c.Error(err)
		// c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}

	log.Println("User added successfully:", newUser.Username)
	c.Status(http.StatusCreated)
}

func (u *UserStorage) ListUsersHandler(c *gin.Context) {
	ctx := c.Request.Context()
	paginationParams := parsePaginationParams(c)

	users, err := u.Store.ListUsers(ctx)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrDatabase.Wrap(err, "error listing users from database"), "Database error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		// c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"meta_data": gin.H{
			"page":      paginationParams.Page,
			"page_size": paginationParams.PageSize,
			"sort_by":   paginationParams.SortBy,
		},
		"data": users,
	})
}

func (u *UserStorage) RefreshTokenHandler(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")
	if tokenString == "" {
		err := errorx.Decorate(models.ErrInvalidRequest.New("authorization header is required"), "Authorization header is required")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tokenString = strings.TrimPrefix(tokenString, "Bearer ")
	newToken, err := utils.RefreshToken(tokenString)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrTokenRefresh.Wrap(err, "error refreshing token"), "Token refresh error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		// c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": newToken})
}

func parsePaginationParams(c *gin.Context) PaginationParams {
	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))
	sortBy := c.Query("sort_by")

	return PaginationParams{
		Page:     page,
		PageSize: pageSize,
		SortBy:   sortBy,
	}
}
