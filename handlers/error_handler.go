package handlers

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
)

type CustomErrorType string

const (
	UNABLE_TO_SAVE          CustomErrorType = "UNABLE_TO_SAVE"
	UNABLE_TO_FIND_RESOURCE CustomErrorType = "UNABLE_TO_FIND_RESOURCE"
	UNABLE_TO_READ          CustomErrorType = "UNABLE_TO_READ"
	UNAUTHORIZED            CustomErrorType = "UNAUTHORIZED"
)

type ErrorInfo struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}

type contextKey string

const (
	errorTypeKey    contextKey = "errorType"
	errorMessageKey contextKey = "errorMessage"
)

func FormatError(errType CustomErrorType) (int, []byte, context.Context) {
	var status int
	var errorMessage string

	switch errType {
	case UNABLE_TO_SAVE:
		status = http.StatusInternalServerError
		errorMessage = "An error occurred while saving data"
	case UNABLE_TO_FIND_RESOURCE:
		status = http.StatusNotFound
		errorMessage = "The requested resource was not found"
	case UNABLE_TO_READ:
		status = http.StatusInternalServerError
		errorMessage = "An error occurred while reading data"
	case UNAUTHORIZED:
		status = http.StatusUnauthorized
		errorMessage = "Unauthorized access"
	default:
		status = http.StatusInternalServerError
		errorMessage = "An unexpected error occurred"
	}

	errorResponse := ErrorInfo{
		Error:   string(errType),
		Message: errorMessage,
	}

	jsonData, _ := json.Marshal(errorResponse)
	ctx := context.WithValue(context.Background(), errorTypeKey, errType)
	ctx = context.WithValue(ctx, errorMessageKey, errorMessage)

	return status, jsonData, ctx
}

func HandleError() gin.HandlerFunc {
	return func(c *gin.Context) {
		if errVal, exists := c.Get(string(errorTypeKey)); exists {
			err := errVal.(CustomErrorType)
			status, jsonData, _ := FormatError(err)
			c.JSON(status, jsonData)
		} else {
			c.Next()
		}
	}
}
