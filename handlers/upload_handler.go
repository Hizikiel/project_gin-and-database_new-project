package handlers

import (
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"third_project/models"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)

var (
	ErrFileCreation = errorx.CommonErrors.NewType("file_creation_error")
	ErrFileWrite    = errorx.CommonErrors.NewType("file_write_error")
)

func (u *UserStorage) UploadImageHandler(c *gin.Context) {
	err := c.Request.ParseMultipartForm(32 << 20)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrInvalidRequest.Wrap(err, "unable to parse multipart form"), "Request parsing error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		return
	}

	// Log the parsed form data and headers
	log.Printf("Form data: %+v", c.Request.MultipartForm)
	log.Printf("Headers: %+v", c.Request.Header)

	var form struct {
		File *multipart.FileHeader `form:"file" binding:"required"`
	}
	if err := c.ShouldBind(&form); err != nil {
		wrappedErr := errorx.Decorate(err, "unable to get image from form")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		return
	}

	uploadDir := "./uploads/"
	if err := os.MkdirAll(uploadDir, os.ModePerm); err != nil {
		wrappedErr := errorx.Decorate(ErrFileCreation.Wrap(err, "unable to create upload directory"), "Directory creation error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		return
	}

	fileName := strconv.FormatInt(time.Now().Unix(), 10) + "-" + form.File.Filename
	filePath := filepath.Join(uploadDir, fileName)
	f, err := os.Create(filePath)
	if err != nil {
		wrappedErr := errorx.Decorate(ErrFileCreation.Wrap(err, "unable to create file for writing"), "File creation error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		return
	}
	defer f.Close()

	uploadFile, err := form.File.Open()
	if err != nil {
		wrappedErr := errorx.Decorate(ErrFileCreation.Wrap(err, "unable to retrieve the io.reader from the file"), "File open error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		return
	}
	defer uploadFile.Close()

	_, err = io.Copy(f, uploadFile)
	if err != nil {
		wrappedErr := errorx.Decorate(ErrFileWrite.Wrap(err, "unable to write file to disk"), "File write error")
		log.Printf("Error: %+v", wrappedErr)
		c.Error(wrappedErr)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": fmt.Sprintf("Image uploaded successfully. Filename: %s", fileName),
	})
}
