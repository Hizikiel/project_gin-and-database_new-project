package handlers

import (
	"log"
	"net/http"
	"path/filepath"
	"third_project/models"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
)

var (
	ErrFileNotFound = errorx.CommonErrors.NewType("file_not_found")
)

func (u *UserStorage) OpenImageHandler(c *gin.Context) {
	fileName := c.Query("filename")
	if fileName == "" {
		err := errorx.Decorate(models.ErrInvalidRequest.New("filename is required"), "Missing filename parameter")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx := c.Request.Context()
	filePath, err := u.Store.GetImagePath(ctx, fileName)
	if err != nil {
		wrappedErr := errorx.Decorate(models.ErrDatabase.Wrap(err, "failed to retrieve image path"), "Database error")
		log.Printf("Error: %+v", wrappedErr)
		c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}

	if filePath == "" {
		err := errorx.Decorate(ErrFileNotFound.New("file not found"), "No file found for the given filename")
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	fullFilePath := filepath.Join("./uploads/", filePath)
	c.File(fullFilePath)
}
