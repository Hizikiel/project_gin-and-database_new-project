package middleware

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type ContextKey string

const (
	RequestIdKey ContextKey = "request"
	UserIdKey    ContextKey = "userId"
)

func RequestIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		requestID := fmt.Sprintf("%d", time.Now().UnixNano())
		c.Set(string(RequestIdKey), requestID)
		c.Next()
	}
}

func UserIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := GenerateUserID()
		c.Set(string(UserIdKey), userID)
		c.Next()
	}
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		_, err := c.Cookie("token")

		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Unauthorized"})
			c.Abort()
			return
		}

		c.Next()
	}
}

func ProtectedHandler(c *gin.Context) {
	requestID, _ := GetRequestID(c)
	userID, _ := GetUserID(c)

	c.String(http.StatusOK, "Protected endpoint accessed!\n")
	c.String(http.StatusOK, "Request ID: %s\n", requestID)
	c.String(http.StatusOK, "User ID: %s\n", userID)
}

var userIDCounter int

func GenerateUserID() string {
	userIDCounter++
	return fmt.Sprintf("%d", userIDCounter)
}

func GetRequestID(c *gin.Context) (string, error) {
	requestID, exists := c.Get(string(RequestIdKey))
	if !exists {
		return "", fmt.Errorf("request ID not found in context")
	}
	return requestID.(string), nil
}

func GetUserID(c *gin.Context) (string, error) {
	userID, exists := c.Get(string(UserIdKey))
	if !exists {
		return "", fmt.Errorf("user ID not found in context")
	}
	return userID.(string), nil
}
