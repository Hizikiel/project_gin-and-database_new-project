package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
	"go.uber.org/zap"
)

var (
	ErrorNamespace     = errorx.NewNamespace("error_handlers")
	ErrInvalidRequest  = ErrorNamespace.NewType("invalid_request")
	ErrDatabase        = ErrorNamespace.NewType("database_error")
	ErrUserExists      = ErrorNamespace.NewType("user_exists")
	ErrUnauthorized    = ErrorNamespace.NewType("unauthorized")
	ErrTokenGeneration = ErrorNamespace.NewType("token_generation_error")
	ErrTokenRefresh    = ErrorNamespace.NewType("token_refresh_error")
	ErrInternalServer  = ErrorNamespace.NewType("internal_server_error")
)

func ErrorMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		logger, err := zap.NewDevelopment()
		if err != nil {
			panic(err)
		}

		defer logger.Sync()
		errors := c.Errors
		var statusCode int
		if len(errors) > 0 {
			for _, e := range errors {
				switch {
				case errorx.IsOfType(e.Err, ErrDatabase), errorx.IsOfType(e.Err, ErrInternalServer), errorx.IsOfType(e.Err, ErrTokenGeneration), errorx.IsOfType(e.Err, ErrTokenRefresh):
					statusCode = http.StatusInternalServerError
				case errorx.IsOfType(e.Err, ErrUnauthorized):
					statusCode = http.StatusUnauthorized
				case errorx.IsOfType(e.Err, ErrInvalidRequest):
					statusCode = http.StatusBadRequest
				case errorx.IsOfType(e.Err, ErrUserExists):
					statusCode = http.StatusConflict
				default:
					statusCode = http.StatusInternalServerError
				}
				c.AbortWithStatusJSON(statusCode, gin.H{"error": e.Error()})

				logger.Error("error",
					zap.Error(e),
				)

			}
		}
	}
}
