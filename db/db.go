package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"path/filepath"
	
	"third_project/models"
)

type Database struct {
	DB *sql.DB
}

func NewDatabase(dsn string) *Database {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	if err := db.Ping(); err != nil {
		log.Fatalf("Failed to ping database: %v", err)
	}

	fmt.Println("Connected to the database")

	return &Database{DB: db}
}

func (d *Database) AddUser(ctx context.Context, user models.User) error {
	query := "INSERT INTO users (username, password, phone_number, address, email) VALUES (?, ?, ?, ?, ?)"
	_, err := d.DB.ExecContext(ctx, query, user.Username, user.Password, user.PhoneNumber, user.Address, user.Email)
	if err != nil {
		err := models.ErrDatabase.Wrap(err, "failed to add user")
		return err
	}
	fmt.Println("User added:", user)
	return nil
}

func (d *Database) GetUser(ctx context.Context, username string) (models.User, bool, error) {
	query := "SELECT id, username, password, phone_number, address, email FROM users WHERE username = ?"
	row := d.DB.QueryRowContext(ctx, query, username)

	var user models.User
	err := row.Scan(&user.ID, &user.Username, &user.Password, &user.PhoneNumber, &user.Address, &user.Email)
	if err != nil {
		if err == sql.ErrNoRows {
			return models.User{}, false, nil
		}
		errorwrapper := models.ErrDatabase.Wrap(err, "user data scan error")
		return models.User{}, false, errorwrapper
	}
	return user, true, nil
}

func (d *Database) GetImagePath(ctx context.Context, fileName string) (string, error) {
	baseDir := "./uploads/"
	filePath := filepath.Join(baseDir, fileName)
	_, err := os.Stat(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			return "", fmt.Errorf("file not found: %s", fileName)
		}
		errorwrapper := models.ErrInvalidRequest.Wrap(err, "error checking file")
		return "", errorwrapper
	}
	return filePath, nil
}

func (d *Database) ListUsers(ctx context.Context) ([]models.User, error) {
	query := "SELECT id, username, password, phone_number, address, email FROM users"
	rows, err := d.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("failed to list users: %w", err)
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var user models.User
		if err := rows.Scan(&user.ID, &user.Username, &user.Password, &user.PhoneNumber, &user.Address, &user.Email); err != nil {
			return nil, fmt.Errorf("failed to scan user: %w", err)
		}
		users = append(users, user)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("row iteration error: %w", err)
	}
	return users, nil
}
