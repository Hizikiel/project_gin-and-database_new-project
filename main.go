package main

import (
	"log"
	"third_project/db"
	"third_project/handlers"
	"third_project/middleware"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {

	dsn := "root:hizikiel@2128@tcp(127.0.0.1:3306)/hizikielo?charset=utf8mb4&parseTime=True&loc=Local"
	db := db.NewDatabase(dsn)

	usersHandler := handlers.NewUserStorage(*db)
	r := gin.Default()
	r.Use(middleware.RequestIDMiddleware())
	r.Use(middleware.Timeout(10 * time.Second))

	r.Use(middleware.ErrorMiddleware())
	r.POST("/register", usersHandler.RegisterHandler)
	r.POST("/login", usersHandler.LoginHandler)
	r.POST("/refresh_token", middleware.AuthMiddleware(), usersHandler.RefreshTokenHandler)
	r.GET("/listusers", middleware.AuthMiddleware(), usersHandler.ListUsersHandler)
	r.POST("/upload", middleware.AuthMiddleware(), usersHandler.UploadImageHandler)
	r.GET("/open", middleware.AuthMiddleware(), usersHandler.OpenImageHandler)

	port := ":8080"
	log.Printf("Starting server on port %s", port)
	if err := r.Run(port); err != nil {
		log.Fatalf("Server failed to start: %v", err)
	}
}
